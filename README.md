## TelegramBotPython

a simple Telegram bot for Night Quest Terminal (https://t.me/NightQuestBot), hosted on https://pythonanywhere.com

`pip install -r requirements.txt`
`python main.py`

---
Source:
https://davidmelash.medium.com/how-to-build-a-simple-telegram-bot-using-python-0e6ee808d03b